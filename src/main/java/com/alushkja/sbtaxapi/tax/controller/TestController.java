package com.alushkja.sbtaxapi.tax.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/test")
@Slf4j
public class TestController {

	@GetMapping
	public ResponseEntity<String> testEndpoint() throws InterruptedException {
		Thread.sleep(100);
		log.info("TestController.testEndpoint()");
		return ResponseEntity.ok("it works! -xxxx " + System.currentTimeMillis());
	}
	
	@GetMapping("/improvements")
	public ResponseEntity<String> improvementsTestEndpoint() throws InterruptedException {
		Thread.sleep(100);
		log.info("TestController.testEndpoint()");
		return ResponseEntity.ok("it works! -xxxx " + System.currentTimeMillis());
	}

}
