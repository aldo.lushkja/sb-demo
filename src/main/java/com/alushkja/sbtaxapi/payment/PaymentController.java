package com.alushkja.sbtaxapi.payment;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@RestController
@RequestMapping("/api/v1/payments")
//@RequiredArgsConstructor
public class PaymentController {

	private final Random random = new Random();

	@GetMapping
	public ResponseEntity<List<Payment>> getPayments(){
		List<Payment> payments = new ArrayList<>();
		for(int i = 0; i < 10; i++){
			final var amount = random.nextDouble();
			final var payment = Payment.builder()
					.id(String.valueOf(i))
					.amount(amount)
					.build();
			payments.add(payment);

		}
		return ResponseEntity.ok(payments);
	}

}
