package com.alushkja.sbtaxapi.payment;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Payment {
	
	@NotBlank(message = "id must be filled correctly")
	public String id;
	
	@Positive(message = "amount must be greater than zero")
	private double amount;
}
