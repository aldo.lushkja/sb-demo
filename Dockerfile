FROM amazoncorretto:11-alpine
MAINTAINER Aldo Lushkja <aldo.lushkja@gmail.com>

ADD ./target/sb-tax-api.jar /app/
CMD ["java", "-Xmx200m", "-jar", "/app/sb-tax-api.jar"]

EXPOSE 8000