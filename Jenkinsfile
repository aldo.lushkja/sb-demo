pipeline {
    agent any
    tools {
        maven "maven_3.6.3"
    }

    stages {
        stage("Checkout"){
            steps{
                checkout([$class: 'GitSCM', branches: [[name: '*/main']], extensions: [], userRemoteConfigs: [[url: 'https://gitlab.com/aldo.lushkja/sb-demo']]])
            }
        }

        stage("Unit tests") {
            steps {
                script {
                    sh "mvn test"
                }
            }
        }

        stage("Build artifact") {
            steps {
                script {
                    sh "mvn clean package -DskipTests"
                }
            }
        }

        stage("Build docker image") {
            steps {
                script {
                    sh "docker build -t alushkja/sb-tax-api ."
                }
            }
        }

        stage("Push image to Hub") {
            steps {
                script {
                    withCredentials([string(credentialsId: 'dockerhub_pwd', variable: 'dockerhub_pwd'), string(credentialsId: 'dockerhub_user', variable: 'dockerhub_user')]) {
                        sh('docker login -u ${dockerhub_user} -p ${dockerhub_pwd}')
                        sh('docker push alushkja/sb-tax-api')
                    }
                }
            }
        }
        stage("Run docker container") {
            steps {
                script {
                    sh('docker rm -f sb-tax-api')
                    sh('docker run -d --name sb-tax-api -p 8000:8000 alushkja/sb-tax-api')
                }
            }
        }
    }

}